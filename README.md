
# Lower Complexity Adaptation for Empirical Entropic Optimal Transport - Code Repository

This repository contains the R-code used for the simulations of the paper ["Lower Complexity Adaptation for Empirical Entropic Optimal Transport"](https://arxiv.org/abs/2306.13580).

The file `sinkhorn.cpp` contains an implementation of the stabilized scaling algorithm from [Schmitzer (2019). Stabilized Sparse Scaling Algorithms for Entropy Regularized Transport Problems. SIAM Journal on Scientific Computing: Vol. 41, Iss. 3.] that can be loaded into `R` via the `Rcpp` package.

The file `LCA.R` contains the methods to run Monte Carlo simulations for empirical EOT. The other files contain the parameters for the different settings as given in the paper:
  - `LCA_1-1.R`: cube setting with squared Euclidean norm,
  - `LCA_1-2.R`: cube setting with 1-norm,
  - `LCA_2.R`: surface setting,
  - `LCA_3.R`: semi-discrete setting,
  - `LCA_4.R`: Sinkhorn divergence setting.

The simulations can be run with the command `Rscript LCA_*.R` where `*` is the corresponding identifier (tested with R version 4.2.0). In each simulation step, realizations of random variables are generated and the empirical EOT cost (or Sinkhorn divergence) is calculated. These results are stored in a CSV file with fields
  - `n`: sample size,
  - `d.1`: first dimension,
  - `d.2`: second dimension,
  - `rep`: repetition number,
  - `eps`: entropic regularization parameter,
  - `cost`: calculated empirical EOT cost (or Sinkhorn divergence),
  - `diff`: L1-error of the first marginal,
  - `step`: number of steps iterated,
  - `var1`: variance of the first optimal potential,
  - `var2`: variance of the second optimal potential,

and can be plotted with the code in the file `plot.R`. For example, after completing the command `Rscript LCA_1-1.R` the file `Data/LCA-1-1.csv` contains the simulation data for the cube setting with squared Euclidean norm. Starting an R session in this folder, the corresponding plot, e.g. for entropic regularization parameter `eps = 1`, can be displayed by executing `source("plot.R"); plot.sd("Data/LCA-1-1.csv", 1.0)`.
