
// [[Rcpp::depends(RcppArmadillo)]]

#define RCPP_ARMADILLO_RETURN_ANYVEC_AS_VECTOR
#include<RcppArmadillo.h>
#include<vector>
#include<algorithm>
#include<cmath>

// check if OpenMP is available
// [[Rcpp::export(rng = false)]]
bool hasOMP()
{
#ifdef _OPENMP
    return true;
#else
    return false;
#endif
}

// set the number of threads for OpenMP
// [[Rcpp::export(rng = false)]]
void setOMP(const int threads)
{
#ifdef _OPENMP
    if (threads > 0)
    {
        omp_set_num_threads(threads);
    }
#endif
}

// calculate the cost matrix with entry i,j being equal to the max-norm between x_i and y_j to the power of q
// x - matrix containing observations x_1, ..., x_n (row-wise)
// y - matrix containing observations y_1, ..., y_m (row-wise)
// q - power
arma::mat costMatrixSup(const arma::mat& x, const arma::mat& y, const double q)
{
    const int n = x.n_rows;
    const int m = y.n_rows;

    arma::mat costm = arma::mat(n, m, arma::fill::none);

    #pragma omp parallel for
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            costm(i, j) = std::pow(arma::max(arma::abs(x.row(i) - y.row(j))), q);
        }
    }

    return costm;
}

// calculate the cost matrix with entry i,j being equal to the p-norm between x_i and y_j to the power of q
// x - matrix containing observations x_1, ..., x_n (row-wise)
// y - matrix containing observations y_1, ..., y_m (row-wise)
// p - determines norm
// q - power
// [[Rcpp::export(rng = false)]]
arma::mat costMatrix(const arma::mat& x, const arma::mat& y, double p, double q)
{
    const int n = x.n_rows;
    const int m = y.n_rows;

    if (std::isinf(p))
    {
        return costMatrixSup(x, y, q);
    }

    arma::mat costm = arma::mat(n, m, arma::fill::none);
    const double u = q / p;

    #pragma omp parallel for
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            costm(i, j) = std::pow(arma::accu(arma::pow(arma::abs(x.row(i) - y.row(j)), p)), u);
        }
    }

    return costm;
}

// calculate the cost matrix with entry i,j being equal to the max-norm between x_i and x_j to the power of q
// x - matrix containing observations x_1, ..., x_n (row-wise)
// q - power
arma::mat costMatrixSymmetricSup(const arma::mat& x, const double q)
{
    const int n = x.n_rows;

    arma::mat costm = arma::mat(n, n, arma::fill::none);

    #pragma omp parallel for
    for (int i = 0; i < n; ++i)
    {
        costm(i, i) = 0;

        for (int j = i + 1; j < n; ++j)
        {
            costm(i, j) = std::pow(arma::max(arma::abs(x.row(i) - x.row(j))), q);
            costm(j, i) = costm(i, j);
        }
    }

    return costm;
}

// calculate the cost matrix with entry i,j being equal to the p-norm between x_i and x_j to the power of q
// x - matrix containing observations x_1, ..., x_n (row-wise)
// p - determines norm
// q - power
// [[Rcpp::export(rng = false)]]
arma::mat costMatrixSymmetric(const arma::mat& x, const double p, const double q)
{
    if (std::isinf(p))
    {
        return costMatrixSymmetricSup(x, q);
    }

    const int n = x.n_rows;

    arma::mat costm = arma::mat(n, n, arma::fill::none);
    const double u = q / p;

    #pragma omp parallel for
    for (int i = 0; i < n; ++i)
    {
        costm(i, i) = 0;

        for (int j = i + 1; j < n; ++j)
        {
            costm(i, j) = std::pow(arma::accu(arma::pow(arma::abs(x.row(i) - x.row(j)), p)), u);
            costm(j, i) = costm(i, j);
        }
    }

    return costm;
}

// calculate the scaling kernel (with absorption of the potential vectors)
// mu    - first weight vector
// nu    - second weight vector
// costm - cost matrix
// eps   - entropic regularization parameter
// phi   - first potential vector
// psi   - second potential vector
arma::mat scalingKernel(const arma::vec& mu, const arma::vec& nu, const arma::mat& costm, double eps,
                        const arma::vec& phi, const arma::vec& psi)
{
    arma::mat k = arma::exp(-(((costm.each_row() - psi.t()).each_col() - phi)) / eps);
    k.each_row() %= nu.t();
    k.each_col() %= mu;
    return k;
}

// stabilized scaling algorithm to approximate optimal EOT potentials phi, psi
// mu    - first weight vector
// nu    - second weight vector
// costm - cost matrix
// eps   - entropic regularization parameter
// niter - maximum number of iterations
// tol   - desired threshold on the L1-error of the first marginal
// tau   - absorption threshold
// phi   - first potential vector
// psi   - second potential vector
// hmu   - first marginal of the induced transport plan
// diff  - L1-error of the first marginal
// step  - number of steps iterated
void sinkhorn(const arma::vec& mu, const arma::vec& nu, const arma::mat& costm,
              double eps, double niter, double tol, double tau,
              arma::vec& phi, arma::vec& psi, arma::vec& hmu, double& diff, int& step)
{
    // scaling vectors
    arma::vec u = arma::ones(mu.n_elem);
    arma::vec v = arma::ones(nu.n_elem);

    // kernel
    arma::mat k = scalingKernel(mu, nu, costm, eps, phi, psi);

    for (step = 0; step < niter; ++step)
    {
        // do two steps
        u = mu / (k * v);
        v = nu / (k.t() * u);

        // absorb if one entry in scaling vectors is too big
        if (arma::max(u) > tau || arma::max(v) > tau)
        {
            phi += eps * arma::log(u);
            psi += eps * arma::log(v);

            u = arma::ones(mu.n_elem);
            v = arma::ones(nu.n_elem);

            k = scalingKernel(mu, nu, costm, eps, phi, psi);
        }

        // check marginal error
        if (step % 10 == 0)
        {
            hmu = u % (k * v);

            diff = arma::accu(arma::abs(hmu - mu));
            if (diff <= tol)
            {
                break;
            }
        }
    }

    // final step
    u = mu / (k * v);

    phi += eps * arma::log(u);
    psi += eps * arma::log(v);
}

// calculate EOT for a number of epsilon via epsilon scaling
// mu    - first weight vector
// nu    - second weight vector
// costm - cost matrix
// eps   - entropic regularization parameters
// niter - maximum number of iterations
// tol   - desired threshold on the L1-error of the first marginal
// tau   - absorption threshold
// [[Rcpp::export(rng = false)]]
Rcpp::List sinkhornEpsilonScaling(const arma::vec& mu, const arma::vec& nu, const arma::mat& costm,
                                  const Rcpp::NumericVector& eps, double niter, double tol, double tau)
{
    const int n = eps.size();

    // store results for all epsilon
    std::vector<double> costs(n);
    std::vector<double> diffs(n);
    std::vector<int> steps(n);
    std::vector<double> var1s(n);
    std::vector<double> var2s(n);

    arma::vec hmu;
    double diff;
    int step;

    // start potentials = 0
    arma::vec phi = arma::zeros(mu.n_elem);
    arma::vec psi = arma::zeros(nu.n_elem);

    // epsilon scaling
    for(int i = 0; i < n; ++i)
    {
        sinkhorn(mu, nu, costm, eps[i], niter, tol, tau, phi, psi, hmu, diff, step);

        // EOT cost
        costs[i] = arma::accu(hmu % phi) + arma::accu(nu % psi);
        // L1-error of the first marginal
        diffs[i] = diff;
        // number of steps iterated
        steps[i] = step;
        // sample variance of the first potential vector
        var1s[i] = arma::var(phi);
        // sample variance of the second potential vector
        var2s[i] = arma::var(psi);
    }

    return Rcpp::List::create(Rcpp::Named("cost") = costs, Rcpp::Named("diff") = diffs, Rcpp::Named("step") = steps,
                              Rcpp::Named("var1") = var1s, Rcpp::Named("var2") = var2s);
}

// calculate Sinkhorn divergence for a number of epsilon via epsilon scaling
// mu      - first weight vector
// nu      - second weight vector
// samples - list with sample matrices x, y; powers p, q for the cost matrix; and number d the cost matrix is normalized with
// eps     - entropic regularization parameters
// niter   - maximum number of iterations
// tol     - desired threshold on the L1-error of the first marginal
// tau     - absorption threshold
// [[Rcpp::export(rng = false)]]
Rcpp::List sinkhornDivergenceEpsilonScaling(const arma::vec& mu, const arma::vec& nu, const Rcpp::List& samples,
                                            const Rcpp::NumericVector& eps, const double niter, double tol, double tau)
{
    const int n = eps.size();

    // extract for calculation of cost matrices
    const arma::mat& x = Rcpp::as<arma::mat>(samples["x"]);
    const arma::mat& y = Rcpp::as<arma::mat>(samples["y"]);
    const double p = Rcpp::as<double>(samples["p"]);
    const double q = Rcpp::as<double>(samples["q"]);
    const double d = Rcpp::as<double>(samples["d"]);

    // store results for all epsilon
    std::vector<double> costs(n);
    std::vector<double> diffs(n);
    std::vector<int> steps(n);

    // calculate the three EOT costs one after another via epsilon scaling
    // and store results in the vectors
    {
        arma::mat costm = costMatrix(x, y, p, q) / d;

        Rcpp::List res = sinkhornEpsilonScaling(mu, nu, costm, eps, niter, tol, tau);

        const Rcpp::NumericVector& partCosts = res["cost"];
        const Rcpp::NumericVector& partDiffs = res["diff"];
        const Rcpp::IntegerVector& partSteps = res["step"];

        for (int i = 0; i < n; ++i)
        {
            costs[i] = partCosts[i];
            diffs[i] = partDiffs[i];
            steps[i] = partSteps[i];
        }
    }
    {
        arma::mat costm = costMatrixSymmetric(x, p, q) / d;

        Rcpp::List res = sinkhornEpsilonScaling(mu, mu, costm, eps, niter, tol, tau);

        const Rcpp::NumericVector& partCosts = res["cost"];
        const Rcpp::NumericVector& partDiffs = res["diff"];
        const Rcpp::IntegerVector& partSteps = res["step"];

        for (int i = 0; i < n; ++i)
        {
            costs[i] -= partCosts[i] / 2;
            diffs[i] = std::max(diffs[i], partDiffs[i]);
            steps[i] = std::max(steps[i], partSteps[i]);
        }
    }
    {
        arma::mat costm = costMatrixSymmetric(y, p, q) / d;

        Rcpp::List res = sinkhornEpsilonScaling(nu, nu, costm, eps, niter, tol, tau);

        const Rcpp::NumericVector& partCosts = res["cost"];
        const Rcpp::NumericVector& partDiffs = res["diff"];
        const Rcpp::IntegerVector& partSteps = res["step"];

        for (int i = 0; i < n; ++i)
        {
            costs[i] -= partCosts[i] / 2;
            diffs[i] = std::max(diffs[i], partDiffs[i]);
            steps[i] = std::max(steps[i], partSteps[i]);
        }
    }

    return Rcpp::List::create(Rcpp::Named("cost") = costs, Rcpp::Named("diff") = diffs, Rcpp::Named("step") = steps);
}
